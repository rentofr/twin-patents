This project provides the code to replicate data retrieval for the JRC report below:

>Jindra, B. and Leusin, M., The development of digital sustainability technologies by top RandD investors**, Publications Office of the European Union, Luxembourg, 2022, ISBN 978-92-76-56422-5, [doi](https://dx.doi.org/10.2760/150239), JRC130480.

*Abstract* The report offers a novel approach to identify patents associated with digital sustainability technologies, which combine components of digital technologies with technologies that are relevant for climate change mitigation or adaption. We propose an identification strategy based upon six search modules, which combine specialists’ opinion, keywords and classification-based approaches.

We generate two datasets. For the first, we use PATSTAT 2019a to identify 319,243 patents associated with digital sustainability technologies. We use this dataset to evaluate the accuracy of the proposed strategy in finding technologies that combine both a digital and a sustainable aspect. We find an accuracy above 95.5% for all search modules implemented in the proposed strategy. For the second dataset, we implement the proposed strategy to update the results using PATSTAT 2021b. To make the results more comparable to the EU climate neutrality report 2021 edition, we focus on the period from 2016 to 2018 and filter priority patents using the IP5 strategy. We link the retrieved patents to R&D Scoreboard companies using the JRC-OECD COR&DIP© v.3 dataset. We identify for all R&D Scoreboard companies 325,508 patents in total, from which 5,057 are digital sustainability patents.

The main files are:

1. *Supplementary1_Queries.docx* - This file contains all the queries used to create the final dataset containing patents related to digital sustainability technologies. These queries are written in SQL language and were run in a Microsoft SQL Server containing PATSTAT 2019 (spring version). They capture a total of 319,243 unique patents linked to digital sustainability technologies.
2. *Supplementary2_CPCtranslation.docx* - File with information to translate IPC codes used to create Modules 5 and 6 to their equivalent CPC codes. 
3. *Final_code_upload.R* - R code used to create all figures and descriptives from the paper
